import axios from "axios";

const base_url = 'https://api.open-meteo.com/v1/forecast';

export default async function getWeatherInfo({latitude, longitude, timezone}) {
  let weather = null;
  try{
    let response = await axios.get(base_url, {
      params: {
        latitude: latitude,
        longitude: longitude,
        timezone: timezone || 'auto',
        current_weather: true,
        daily: 'temperature_2m_max,temperature_2m_min',
        temperature_unit: 'fahrenheit'
      }
    });
    weather = {
      weathercode: response.data?.current_weather?.weathercode,
      temperature: response.data?.current_weather?.temperature,
      windspeed: response.data?.current_weather?.windspeed,
      minTemp: response.data?.daily?.temperature_2m_min[0],
      maxTemp: response.data?.daily?.temperature_2m_max[0]
    }
  } catch(err) {
    console.log(err);
    weather = {
      error: true,
      reason: err.toString()
    }
  }
  return weather;
}
