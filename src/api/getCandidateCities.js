import getGeoInfo from "./getGeoInfo";

async function getCandidateCities(queryStr) {
  let locations = [];
  if (!queryStr) return locations;
  locations = await getGeoInfo(queryStr, 6);
  return locations;
}

export default getCandidateCities;