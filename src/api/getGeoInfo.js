import axios from "axios";

const base_url = 'https://geocoding-api.open-meteo.com/v1/search';

export default async function getGeoInfo(name, count) {
  let locations = [];
  try {
    let res = await axios.get(base_url, {
      params: {
        name: name,
        count: count || 1
      }
    });
    
    locations = res.data?.results?.map(res => 
      ({
        id: res.id,
        label: `${res.name}, ${res.admin1}, ${res.country_code}`,
        city: res.name,
        latitude: res.latitude.toFixed(2),
        longitude: res.longitude.toFixed(2),
        country: res.country,
        countryCode: res.country_code,
        admin: res.admin1,
      })
    );
  }
  catch (err) {
    console.log(err);
  }
  return locations || [];
}