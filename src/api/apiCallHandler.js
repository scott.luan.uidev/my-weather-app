import getGeoInfo from "./getGeoInfo";
import getWeatherInfo from "./getWeatherInfo";
import parseQuery from "../utils/parseQuery";

const cache = new Map(); // a naiive cache

export default async function apiCallHandler(location) {
  if (!location) return;

  location = parseQuery(location);
  if (location.type && (location.type === 'zip' || location.type === 'city')) {
    let locations = await getGeoInfo(location.city || location.zip);
    if (locations.length === 0) throw Error('No matching results.'); 
    location = locations[0];
  }
  
  const key = `${location.latitude}, ${location.longitude}`;
  let timeStamp = Date.now();
  if (cache.has(key) && timeStamp - cache.get(key)[1] <= 3600000) {
    return {weather: cache.get(key)[0], location };
  }

  let weather = await getWeatherInfo(location);
  cache.set(key, [weather, timeStamp]);
  return {weather, location};
}