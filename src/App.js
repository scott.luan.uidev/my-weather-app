import { useState } from 'react';
import CurrentWeather from './components/CurrentWeather';
import InputTabs from './components/InputTabs';
import './App.css';

function App() {

  const [currentWeather, setCurrentWeather] = useState(null);
  const [savedLocations, setSavedLocations] = useState([]);

  return (
    <div className="App">
      <h1>My Weather App</h1>
      <section>
        <InputTabs savedLocations={savedLocations} setCurrentWeather={setCurrentWeather}/>
        <CurrentWeather 
          currentWeather={currentWeather} 
          savedLocations={savedLocations} 
          setSavedLocations={setSavedLocations}
        />
      </section>
    </div>
  );
}

export default App;
