import { useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import ApiForm from './Form';
import SavedLocationsTab from './SavedLocationsTab';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function InputTabs({setCurrentWeather, savedLocations}) {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" centered>
          <Tab label="Search" {...a11yProps(0)} />
          <Tab label="Saved Locations" {...a11yProps(1)} disabled={savedLocations.length === 0} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <ApiForm setCurrentWeather={setCurrentWeather}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <SavedLocationsTab 
          savedLocations={savedLocations} 
          setCurrentWeather={setCurrentWeather}
        />
      </TabPanel>
    </div>
  );

}