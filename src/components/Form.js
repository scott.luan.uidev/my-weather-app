import { useState } from 'react';
import apiCallHandler from '../api/apiCallHandler';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button'
import Autocomplete from '@mui/material/Autocomplete';
import useSearch from '../hooks/useSearch';

export default function ApiForm({setCurrentWeather}) {
  const [selected, setSelected] = useState(null);
  const [errorMsg, setErrorMsg] = useState('');
  const {query, setQuery, options} = useSearch();

  const handleInputChange = async (e) => {
    const queryStr = e.target.value;
    setQuery(queryStr);
    setSelected(null);
    setErrorMsg('');
  }

  const handleValueChange = (e, v) => {
    setSelected(v);
    handleSubmit(null, v);
    setErrorMsg('');
  }

  const handleSubmit = async (e, selectedCity) => {
    e?.preventDefault && e.preventDefault();
    if (!selectedCity) return;
    setCurrentWeather({isLoading: true});
    try {
      selectedCity = selected? selected: selectedCity;
      const weather = await apiCallHandler(selectedCity? selectedCity:query);
      // supply additional city information if the user selected a city from options
      setCurrentWeather(weather);
    }
    catch (err) {
      setErrorMsg(err.toString());
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="search-autocomplete">Search City, Zipcode, or Latitude and Longitude</label>
      <Autocomplete 
        id='search-autocomplete'
        freeSolo
        options={options}
        fullWidth
        onInputChange={handleInputChange}
        onChange={handleValueChange}
        filterOptions={x => x}
        renderOption={
          (props, option) => <li {...props} key={option.id}>{option.label}</li>
        }
        renderInput={
          (params) => <TextField {...params} placeholder='e.g. "New York", "85004", or "52, 13"'  />
        }
      />
      <Button variant='contained' onClick={handleSubmit}>Get Current Weather</Button>
      {
        errorMsg
        ?<div><span className='error-message'>{errorMsg}</span></div>
        :null
      }
    </form>
  );
}