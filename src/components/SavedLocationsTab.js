import apiCallHandler from '../api/apiCallHandler';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

export default function SavedLocationsTab({savedLocations, setCurrentWeather}) {
  const handleSelect = async (e) => {
    try {
      const data = await apiCallHandler(savedLocations.find((loc) => loc.label === e.target.value));
      setCurrentWeather(data);
    } catch(err) { // not really expecting error here
      console.log(
        (err.response?.data?.reason)
        ?err.response.data.reason
        :err.toString()
      );
    }
  }

  return (
    <div className="tab">
      <label htmlFor='selectLocation'>Select a saved location</label>
      <Select id='selectLocation' defaultValue={'default'} onChange={handleSelect}>
        <MenuItem disabled value='default'>Please select a location.</MenuItem>
        {savedLocations.map(loc => {
          return <MenuItem key={loc.label} value={loc.label}>{loc.label}</MenuItem>
        })}
      </Select>
    </div>
  )
}