import { WEATHER_CODES } from "../utils/constants";
import Button from "@mui/material/Button";

export default function CurrentWeather({currentWeather, savedLocations, setSavedLocations}) {
  if (!currentWeather) {
    return null;
  }
  if (currentWeather.isLoading) {
    return (    
    <div className="currentWeather">
      <h2>Weather Result:</h2>
      <h3>Loading</h3>
    </div>);
  }

  const {location, weather} = currentWeather;

  const handleSaveLocation = () => {
    setSavedLocations((arr) => arr.concat(location));
  }

  const handleRemoveLocation = () => {
    setSavedLocations(
      (arr) => arr.filter(
        loc => loc.label !== location.label
      )
    );
  }

  
  return (
    <div className="currentWeather">
      <h2>Weather Result:</h2>
      <ul>
        <li>
          {`Location: ${location.label}`}
        </li>
        <li>{`Weather: ${WEATHER_CODES[weather?.weathercode]}`}</li>
        <li>{`Current Temperature: ${weather?.temperature}`}</li>
        <li>{`Low: ${weather?.minTemp} High: ${weather?.maxTemp}`}</li>
        <li>{`Wind speed: ${weather?.windspeed}`}</li>
      </ul>
      {
        (savedLocations.findIndex((loc) => loc.label === location.label) === -1)
        ?<Button onClick={handleSaveLocation} variant="contained">Save This Location</Button>
        :<Button onClick={handleRemoveLocation} variant="contained">Remove This Location</Button>
      }
    </div>
  )
}