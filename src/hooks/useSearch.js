import { useState, useEffect, useRef} from 'react';
import getCandidateCities from '../api/getCandidateCities';
import { debounce } from 'lodash';

export default function useSearch() {
  const [query, setQuery] = useState('');
  const [options, setOptions] = useState([]);
  const handleSearch = useRef(debounce(async (str) => {
    setOptions(await getCandidateCities(str))
  }, 300));

  useEffect(() => {
    if (!query){
      setOptions([]);
    } else {
      handleSearch.current.cancel();
      handleSearch.current(query);
    }
  }, [query, setOptions]);

  return { query, options, setQuery};
}