// naiive query parser
export default function parseQuery(query) {
  if (typeof query === 'object') return query;
  const nums = getNums(query);
  if (// parse the query as latitude and longitude pair
    nums.length >= 2 &&
    nums[0] >= -90 &&
    nums[0] <= 90 &&
    nums[1] >= -180 &&
    nums[1] <= 180
  ) {
    return {
      type: 'coord', 
      latitude: nums[0].toFixed(2), 
      longitude: nums[1].toFixed(2), 
      label: `Latitude: ${nums[0].toFixed(2)}, Longitude: ${nums[1].toFixed(2)}` 
    }
  }

  if (// parse the query as zip code
    nums.length === 1 && nums[0] === parseInt(nums[0]) 
  ) {
    return {type:'zip', zip: nums[0]}
  }

  if (nums.length === 0) { // parse the query as city name
    return {type:'city', city: query}
  }

  return {error: true, reason: 'cannot parse query'};
}


function getNums(s) {
  const arr = s.match(/[-+]?[0-9]*\.?[0-9]+/g) || []; 
  // regex from https://stackoverflow.com/questions/3977256/javascript-how-to-extract-multiple-numbers-from-a-string
  return arr.map(a => parseFloat(a)).filter(a => !isNaN(a) );
}