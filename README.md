# Readme for My-Weather-App

## Setup
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can have a dev version of this app by following these steps.

### 1. Clone the repository.
https://gitlab.com/scott.luan.uidev/my-weather-app/

### 2. Have npm installed.

### 3. Run `npm install`.
Installs the required packages and dependencies.

### 4. Run `npm start`.
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Notes

Completed Features:
- Show weather by location.
- Search with city name or postal code.
- Remember your favourite cities or locations.

Features Remaining:
- Display weather not only on the list but on a map (Google maps?).
- Resolving Latitude&Longitude to nearest city and vice versa.

Thoughts:
- I decided to have only one input element for all three kinds of inputs, coords, city name, and postal code, because I feel it is more natural this way. I wrote a rather naive helper function to decide what type of input the user typed.
- I used a combination of Map and timeStamp to cache fetched data, so that the same request won't be made within one hour. There are many parts that can be improved regarding this.
- I used a few Material UI components for the app. While I have less control over the styles, but they are quick to implement and consistant in style. 


